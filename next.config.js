/** @type {import('next').NextConfig} */
const { i18n } = require('./next-i18next.config');
const withPWA = require('next-pwa');
const runtimeCaching = require('next-pwa/cache');
const withPlugins = require("next-compose-plugins");


const plugins = [
  [
    withPWA,
    {
      pwa: {
        dest: 'public',
        runtimeCaching
      }
    }
  ]
];
const nextConfig = {
  reactStrictMode: true,
  i18n,
  swcMinify: true,
  env: {
    SITE_URL: 'https://arash-pilechi.vercel.app',
    NEXT_PUBLIC_GA_ID: 'G-F3BZLKPSWN'
  },
  publicRuntimeConfig: {
    pwa:{
      name: "Arash Pilechi's Resume",
      theme: "#00222a",
      logo: "/images/pwa/192.png",
      types: {
        'ico': "/images/pwa/icon.png",
        'svg': "/images/pwa/svg.png"
      },
      sizes: {
        '16': "/images/pwa/16.png",
        '32': "/images/pwa/32.png",
        '72': "/images/pwa/72.png",
        '120': "/images/pwa/120.png",
        '144': "/images/pwa/144.png",
        '180': "/images/pwa/180.png"
      }
    }
  }
}

module.exports = withPlugins(plugins, nextConfig);
