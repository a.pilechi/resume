import {useState} from 'react'
import {useTranslation} from "next-i18next";
import ContactInfo from "@/public/data/contactInfo.json";
import SocialMedia from "@/public/data/socialMedia.json";
import Samples from "@/public/data/samples.json";
import Specialties from "@/public/data/specialties.json";

export const CommonJsonLd = () => {
    const {t} = useTranslation('common');
    const [samples] = useState(() => {
        let works = [];
        let projects = [];
        Samples.map(sample => {
            if(sample?.job){
                works.push(sample);
            }
            if(sample?.project){
                projects.push(sample);
            }
        })
        return{
            works,
            projects
        }
    });
    return {
        __html: `{
                "@context": "http://www.schema.org",
                 "basics": {
                    "name": "${t('info.name')}",
                    "label": "${t('info.job_title')}",
                    "image": "https://arash-pilechi.vercel.app/images/profile.jpg",
                    "email": "${ContactInfo[1].text}",
                    "phone": "${ContactInfo[0].text}",
                    "birthDate": "${t('info.birthDate')}",
                    "nationality": "${t('info.nationality')}",
                    "url": "https://arash-pilechi.vercel.app/",
                    "summary": "${t('info.summary')}",
                    "location": {
                      "city": "${t('info.city')}",
                      "countryCode": "IR",
                      "region": "${t('info.country')}"
                    },
                    "profiles": [${SocialMedia.map(({network,username,url}) => {return JSON.stringify({network,username,url})})}]
                  },
                  "work": ${JSON.stringify(samples.works?.map(sample => {return {
                    "name": t(`samples.${sample.id}.name`),
                    "position": t(`samples.${sample.id}.position`),
                    "url": sample.address || "''",
                    "startDate": t(`samples.${sample.id}.start_date`),
                    "endDate": t(`samples.${sample.id}.end_date`).length ? t(`samples.${sample.id}.end_date`) : "",
                    "summary": t(`samples.${sample.id}.describe`),
                    "highlights": [
                        t(`samples.${sample.id}.position`)
                    ]
                    }}))},
                  "education": [{
                    "institution": "${t('education.institution')}",
                    "area": "${t('education.area')}",
                    "studyType": "${t('education.study_type')}",
                    "startDate": "${t('education.start_date')}",
                    "endDate": "${t('education.end_date')}"
                  }],
                  "skills": ${JSON.stringify(Object.values(Specialties).map(speciality => {return {
                        "name": speciality.text,
                        "level": speciality.percent > 80 ? "Master" : (speciality.percent > 50 ? "Medium" : "Beginner"),
                        "keywords": [speciality.text]
                    }}))},
                  "languages": [{
                    "language": "Persian",
                    "fluency": "Native speaker"
                  },{
                    "language": "English",
                    "fluency": "Proficient"
                  }],
                  "projects": ${JSON.stringify(samples.projects?.map(sample => {return {
                    "name": t(`samples.${sample.id}.name`),
                    "description": t(`samples.${sample.id}.describe`),
                    "highlights": [
                        t(`samples.${sample.id}.position`)
                    ],
                    "keywords": sample.technologies,
                    "startDate": t(`samples.${sample.id}.start_date`),
                    "endDate": t(`samples.${sample.id}.end_date`).length ? t(`samples.${sample.id}.end_date`) : "",
                    "url": sample.address || "''",
                    "roles": [t(`samples.${sample.id}.position`)],
                    "entity": sample.status === 'online' ? "Entity" : "",
                    "type": sample.type
                }}))}
              }
          `,
    };
}