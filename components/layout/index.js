import {useTranslation} from 'next-i18next'
import Header from '@/header';
import Footer from '@/footer';

const LayoutDefault = ({children}) => {
    const { t } = useTranslation('common');
    return(
        <>
            <Header/>
            {children}
            <Footer />
            <noscript><p>{t('common:noScript')}</p></noscript>
        </>
    )
}
export default LayoutDefault