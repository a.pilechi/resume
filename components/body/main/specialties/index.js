import Title from './components/title'
import Specialities from './components/specialties'

const Index = () => {
    return(
        <main>
            <section className="container">
                <Title />
                <Specialities />
            </section>
        </main>
    )
}
export default Index