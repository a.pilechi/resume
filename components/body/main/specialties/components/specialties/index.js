import Specialties from '@/public/data/specialties.json'
import Card from './card'
import styles from './index.module.scss'

const Index = () => {
    return(
        <div className={styles.specialities}>
            {
                Object.values(Specialties).map( (speciality, index)  =>
                    <Card initialData={speciality} key={index} />
                )
            }
        </div>
    )
}
export default Index