import {useTranslation} from 'next-i18next'
import styles from './index.module.scss'

const Index = ({status}) => {
    const { t } = useTranslation('samples');
    return(
        <header className={styles.title}>
            <h1>{t(`title.${status}`)}</h1>
        </header>
    )
}
export default Index