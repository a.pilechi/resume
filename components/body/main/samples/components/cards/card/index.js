import Link from 'next/link'
import Image from 'next/image'
import {useTranslation} from 'next-i18next'
import {angle_left} from "@/svg";
import styles from './index.module.scss'

const Index = ({initialData,status}) => {
    const { t } = useTranslation('common');
    if(!initialData?.project) return null
    return(
        <article className={styles.sample}>
            <Link href={`/samples/${status}/${initialData.id}`}>
                <div className={styles.logo}>
                    <figure className={styles.holder}>
                        <Image src={`/images/samples/${initialData.id}/logo.png`}
                               alt="img"
                               width={50}
                               height={50}
                        />
                    </figure>
                </div>
                <div className={styles.triangle} />
                <div className={styles.content}>
                    <div className={styles.title}>
                        <h2>{t(`samples.${initialData.id}.name`)}</h2>
                    </div>
                    <div className={styles.holder}>
                        <div className={styles.describe}>
                            <p>{t(`samples.${initialData.id}.describe`).substring(0,100)}{t(`samples.${initialData.id}.describe`).length > 100 ? ' ...' : ''}</p>
                        </div>
                        <div className={styles.buttons}>
                            <button className={`${styles.button} custom_button`} type="button">
                                <div className={styles.text}>
                                    <span>{t('view')}</span>
                                </div>
                                <div className={`${styles.icon} mirror`}>
                                    {angle_left()}
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </Link>
        </article>
    )
}
export default Index