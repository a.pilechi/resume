import Title from './components/title'
import Cards from './components/cards'

const Index = () => {
    return(
        <main>
            <section className="container">
                <Title status="online" />
                <Cards status="online" />
            </section>
            <section className="container">
                <Title status="offline" />
                <Cards status="offline" />
            </section>
        </main>
    )
}
export default Index