import Link from 'next/link'
import {useRouter} from "next/router";
import {useTranslation} from 'next-i18next'
import {angle_right, angle_left} from '@/svg'
import styles from './index.module.scss'

const Index = ({address}) => {
    const {t} = useTranslation('samples');
    const {query: {id}} = useRouter();
    return(
        <div className={styles.buttons}>
            <Link href="/samples">
                <button className={styles.cancel}>
                    <div className={`${styles.icon} mirror`}>
                        {angle_right()}
                    </div>
                    <div className={styles.text}>
                        <span>{t('other_samples')}</span>
                    </div>
                </button>
            </Link>
            {
                address &&
                <Link href={address} rel="nofollow noreferrer" target="_blank" title={t(`samples.${id}.name`)}>
                    <button className={styles.submit}>
                        <div className={styles.text}>
                            <span>{t('view')}</span>
                        </div>
                        <div className={`${styles.icon} mirror`}>
                            {angle_left()}
                        </div>
                    </button>
                </Link>
            }
        </div>
    )
}
export default Index