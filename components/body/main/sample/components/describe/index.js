import {useRouter} from "next/router";
import {useTranslation} from 'next-i18next'
import styles from './index.module.scss'

const Index = () => {
    const {t} = useTranslation('common');
    const {query: {id}} = useRouter();
    return(
        <div className={styles.describe}>
            <h2>{t(`samples.${id}.describe`)}</h2>
        </div>
    )
}
export default Index