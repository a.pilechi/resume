import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";
import Specialties from '@/public/data/specialties.json'
import * as SVG from "@/svg"
import styles from './index.module.scss'

const Index = ({technologies}) => {
    const {t} = useTranslation('samples');
    const {query: {id}} = useRouter();
    return(
        <div className={styles.technologies} title={t('technologies_title',{joinArrays: ' ', sample: t(`common:samples.${id}.name`)})}>
            {
                technologies?.map( technology =>
                    <div className={styles.technology}
                         title={Specialties[technology].text}
                         key={technology.text}>
                        <div className={`${styles.icon} ${styles[Specialties[technology].icon.name]}`}>
                            {SVG[Specialties[technology].icon.name]()}
                        </div>
                    </div>
                )
            }
        </div>
    )
}
export default Index