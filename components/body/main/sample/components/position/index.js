import {useRouter} from "next/router";
import {useTranslation} from 'next-i18next'
import styles from './index.module.scss'

const Index = () => {
    const {t} = useTranslation('common');
    const {query: {id}} = useRouter();
    return(
        <div className={styles.position}>
            <span>{t('position')}:</span>
            <h3 title={t('position_title',{joinArrays: ' ', name: t('info.name'), company: t(`samples.${id}.name`)})}>{t(`samples.${id}.position`)}</h3>
        </div>
    )
}
export default Index