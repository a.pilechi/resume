import { useState } from 'react';
import {useRouter} from "next/router";
import Image from 'next/image'
import {useTranslation} from 'next-i18next'
import Swiper from "react-id-swiper";
import Lightbox from "yet-another-react-lightbox";
import 'swiper/swiper.scss';
import "yet-another-react-lightbox/styles.css";
import styles from './index.module.scss'

const Index = ({images}) => {
    const {query: {id}} = useRouter();
    const [photoIndex,setPhotoIndex] = useState(0);
    const [open,setOpen] = useState(false);
    const {t} = useTranslation('common');
    const params = {
        /*autoplay: {
            delay: 2500,
            disableOnInteraction: false
        },*/
        lazy: true,
        slidesPerView: 'auto',
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            dynamicBullets: true
        },
    };
    const openInLightBox = index => {
        setPhotoIndex(index);
        setOpen(true);
    }
    return(
        <div className={styles.slider}>
            <Swiper {...params}>
                {
                    images?.map( (image,index) =>
                        <div className={styles.image}
                             onClick={() => openInLightBox(index)}
                             key={index}>
                            <Image src={`/images/samples/${id}/${image.type}/${image.fileName}`}
                                   alt={t(`samples.${id}.name`)}
                                   fill
                                   objectPosition="center"
                                   objectFit="contain"
                            />
                        </div>
                    )
                }
            </Swiper>
            <Lightbox
                open={open}
                close={() => setOpen(false)}
                slides={images?.map(image => {return {src: `/images/samples/${id}/${image.type}/${image.fileName}`}})}
            />
        </div>
    )
}
export default Index