import {useRouter} from "next/router";
import Image from "next/image"
import {useTranslation} from 'next-i18next'
import styles from './index.module.scss'

const Index = ({address, related}) => {
    const {t} = useTranslation('common');
    const {query: {status, id}} = useRouter();
    return(
        <header className={styles.title}>
            <div className={styles.logo}>
                <Image src={`/images/samples/${id}/logo.png`}
                       alt="img"
                       width={40}
                       height={40}
                />
            </div>
            {
                related || status === 'offline'
                ?
                    <div className={styles.text}>
                        <h1>{t(`samples.${id}.name`)}</h1>
                    </div>
               :
                    <a href={address} rel="noreferrer" target="_blank" title={t(`samples.${id}.name`)}>
                        <div className={styles.text}>
                            <h1>{t(`samples.${id}.name`)}</h1>
                        </div>
                    </a>
            }
        </header>
    )
}
export default Index