import {useRouter} from "next/router";
import {useTranslation} from 'next-i18next'
import styles from './index.module.scss'

const Index = ({related}) => {
    const {t} = useTranslation('samples');
    const {query: {id}} = useRouter();
    if(!related) return null
    return(
        <div className={styles.related}>
            <div className={styles.title}>
                <p>{t('related')}:</p>
            </div>
            <div className={styles.list}>
                {
                    related.map( item =>
                        <div className={styles.item} key={item.id}>
                            <a href={item.address} rel="nofollow noreferrer" target="_blank">
                                <p>{t(`common:samples.${id}.related.${item.id}`)}</p>
                            </a>
                        </div>
                    )
                }
            </div>
        </div>
    )
}
export default Index