import {useMemo} from "react";
import {useRouter} from "next/router";
import Samples from '@/public/data/samples.json'
import Slider from './components/slider'
import Title from './components/title'
import Describe from './components/describe'
import Position from './components/position'
import Technologies from './components/technologies'
import Related from './components/related'
import Buttons from "./components/buttons";
import styles from './index.module.scss'

const Index = () => {
    const {query: {status,id}} = useRouter();
    const initialData = useMemo(() => Samples.filter(sample => sample.id === id)[0], [status,id]);
    return(
        <section className={styles.sample}>
            <article className="container">
                <Slider images={initialData?.images} />
                <Title  address={initialData?.address} related={initialData?.related} />
                <Describe />
                <Position />
                {
                    status === 'online' &&
                        <>
                            <Technologies technologies={initialData?.technologies} />
                            <Related related={initialData?.related} />
                        </>
                }
                <Buttons address={initialData?.address} />
            </article>
        </section>
    )
}
export default Index