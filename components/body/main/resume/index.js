import Title from './components/title'
import Cards from './components/cards'

const Index = () => {
    return(
        <main>
            <section className="container">
                <Title />
                <Cards />
            </section>
        </main>
    )
}
export default Index