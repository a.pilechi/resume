import Samples from '@/public/data/samples.json'
import Card from "./card"
import styles from "./index.module.scss"

const Index = ({status}) => {
    return(
        <div className={styles.samples}>
            {
                Samples.filter(sample => sample?.job).map((sample, index) =>
                    <Card initialData={sample} status={status} key={index} />
                )
            }
        </div>
    )
}
export default Index