import { useMemo } from 'react'
import Image from 'next/image'
import {useTranslation} from 'next-i18next'
import styles from './index.module.scss'

const Index = ({initialData}) => {
    const { t } = useTranslation('common');
    const start_date = useMemo(() => {
        const temp = t(`samples.${initialData.id}.start_date`);
        const date = temp?.split('-');
        return `${date[0]}/${date[1]}`
    },[initialData.id])
    const end_date = useMemo(() => {
        const temp = t(`samples.${initialData.id}.end_date`);
        if(temp?.length){
            const date = temp?.split('-');
            return `${date[0]}/${date[1]}`
        } else {
            return t('until_today')
        }
    },[initialData.id])
    return(
        <article className={styles.jobs}>
            <div className={styles.segment}>
                <div className={styles.logo}>
                    <figure className={styles.holder}>
                        <Image src={`/images/jobs/${initialData.id}.png`}
                               alt="img"
                               width={50}
                               height={50}
                        />
                    </figure>
                </div>
                <div className={styles.title}>
                    <h2>{t(`samples.${initialData.id}.name`)}</h2>
                </div>
            </div>
            <div className={styles.segment}>
                <div className={styles.info}>
                    <span>{t('position')}:</span>
                    <span title={t('position_title',{joinArrays: ' ', name: t('info.name'), company: t(`samples.${initialData.id}.name`)})}>{t(`samples.${initialData.id}.position`)}</span>
                </div>
                <div className={styles.info}>
                    <span>{t('cooperation_date')}:</span>
                    <span>{t('date_from_to',{joinArrays: ' ', start_date , end_date})}</span>
                </div>
                <div className={styles.describe}>
                    <p>{t(`samples.${initialData.id}.describe`).substring(0,100)}{t(`samples.${initialData.id}.describe`).length > 100 ? ' ...' : ''}</p>
                </div>
            </div>
        </article>
    )
}
export default Index