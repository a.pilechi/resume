import {useTranslation} from 'next-i18next'
import styles from './index.module.scss'

const Index = () => {
    const { t } = useTranslation('resume');
    return(
        <header className={styles.title}>
            <h1>{t('title')}</h1>
        </header>
    )
}
export default Index