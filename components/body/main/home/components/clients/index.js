import {useMemo} from 'react'
import Link from 'next/link'
import Image from 'next/image'
import {useTranslation} from 'next-i18next'
import Swiper from 'react-id-swiper'
import Samples from '@/public/data/samples.json'
import {angle_left} from '@/svg'
import 'swiper/swiper.scss';
import styles from './index.module.scss'
import * as SVG from "@/svg";

const Index = () => {
    const {t} = useTranslation('home');
    const params = {
        autoplay: {
            delay: 2500,
            disableOnInteraction: false
        },
        breakpoints: {
            1024: {
                slidesPerView: 6,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 4,
                spaceBetween: 10
            },
            320: {
                slidesPerView: 3,
                spaceBetween: 10
            }
        }
    }
    const clients = useMemo(() => {
        const temp = [];
        Samples.forEach(sample => {
            if(sample.favorite) {
                temp.push(sample);
            }
        });
        return temp;
    },[]);
    return(
        <section className={styles.clients}>
            <div className={styles.slider}>
                <Swiper {...params}>
                    {
                        clients.map(sample =>
                            <div className={styles.client} key={sample.id}>
                                <Link href={`/samples/${sample.status}/${sample.id}`} title={t(`common:samples.${sample.id}.name`)}>
                                    <Image src={`/images/samples/${sample.id}/logo.png`}
                                           alt="img"
                                           width={50}
                                           height={50}
                                    />
                                </Link>
                            </div>
                        )
                    }
                </Swiper>
            </div>
            <div className={styles.moreBox}>
                <Link href="/samples" title={t('samples_title',{joinArrays: ' ', name: t('name')})}>
                    <button className={`${styles.more} custom_button`} type="button">
                        <div className={styles.text}>
                            <span>{t('see_samples')}</span>
                        </div>
                        <div className={`${styles.icon} mirror`}>
                            {angle_left()}
                        </div>
                    </button>
                </Link>
            </div>
        </section>
    )
}
export default Index