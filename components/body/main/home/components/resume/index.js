import {useMemo} from 'react'
import Link from 'next/link'
import Image from 'next/image'
import {useTranslation} from 'next-i18next'
import Swiper from 'react-id-swiper'
import Samples from '@/public/data/samples.json'
import {angle_left} from '@/svg'
import 'swiper/swiper.scss';
import styles from './index.module.scss'

const Index = () => {
    const {t} = useTranslation('home');
    const params = {
        /*autoplay: {
            delay: 2500,
            disableOnInteraction: false
        },*/
        breakpoints: {
            1024: {
                slidesPerView: 6,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 4,
                spaceBetween: 10
            },
            320: {
                slidesPerView: 3,
                spaceBetween: 10
            }
        }
    }
    const clients = useMemo(() => {
        const temp = [];
        Samples.forEach(sample => {
            if(sample.job) {
                temp.push(sample);
            }
        });
        return temp;
    },[]);
    return(
        <section className={styles.resume}>
            <div className={styles.slider}>
                <Swiper {...params}>
                    {
                        clients.map(sample =>
                            <div className={styles.client} key={sample.id}>
                                <Image src={`/images/jobs/${sample.id}.png`}
                                       alt="img"
                                       width={50}
                                       height={50}
                                />
                            </div>
                        )
                    }
                </Swiper>
            </div>
            <div className={styles.moreBox}>
                <Link href="/resume" title={t('resume_title',{joinArrays: ' ', name: t('name')})}>
                    <button className={`${styles.more} custom_button`} type="button">
                        <div className={styles.text}>
                            <span>{t('see_resume')}</span>
                        </div>
                        <div className={`${styles.icon} mirror`}>
                            {angle_left()}
                        </div>
                    </button>
                </Link>
            </div>
        </section>
    )
}
export default Index