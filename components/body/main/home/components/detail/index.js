import {useTranslation} from 'next-i18next'
import styles from './index.module.scss'

const Index = () => {
    const {t} = useTranslation('home');
    return(
        <div className={styles.detail}>
            <h2>{t('job_title')}</h2>
            <h3><span>{t('detail1.0')}:</span><span>{t('detail1.1')}</span></h3>
            <h3><span>{t('detail2.0')}:</span><span>{t('detail2.1')}</span></h3>
            <h3><span>{t('detail3.0')}:</span><span>{t('detail3.1')}</span></h3>
            <h3><span>{t('detail4.0')}:</span><span>{t('detail4.1')}</span></h3>
        </div>
    )
}
export default Index