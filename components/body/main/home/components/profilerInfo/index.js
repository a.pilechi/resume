import Image from "next/image"
import {useTranslation} from 'next-i18next'
import styles from './index.module.scss'

const Index = () => {
    const { t } = useTranslation('home');
    return(
        <div className={styles.personalInfo}>
            <div className={styles.image}>
                <figure>
                    <Image src={`/images/profile.jpg`}
                           alt={t('name')}
                           width={200}
                           height={200}
                    />
                </figure>
            </div>
            <div className={styles.name}>
                <h1>{t('name')}</h1>
            </div>
        </div>
    )
}
export default Index