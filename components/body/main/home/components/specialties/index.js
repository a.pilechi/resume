import Link from 'next/link'
import {useTranslation} from 'next-i18next'
import Swiper from "react-id-swiper";
import * as SVG from '@/svg'
import Specialties from '@/public/data/specialties.json'
import 'swiper/swiper.scss';
import styles from './index.module.scss'
import {angle_left} from "@/svg";

const Index = () => {
    const {t} = useTranslation('home');
    const params = {
        autoplay: {
            delay: 2500,
            disableOnInteraction: false
        },
        breakpoints: {
            1024: {
                slidesPerView: 6,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 4,
                spaceBetween: 10
            },
            320: {
                slidesPerView: 3,
                spaceBetween: 10
            }
        }
    }
    return(
        <section className={styles.specialities}>
            <div className={styles.slider}>
                <Swiper {...params}>
                    {
                        Object.values(Specialties).map( (speciality, index)  =>
                            <div className={styles.speciality} key={index}>
                                <div className={`${styles.icon} ${styles[speciality.icon.name]}`}
                                     title={speciality.text}>
                                    {SVG[speciality.icon.name]()}
                                </div>
                            </div>
                        )
                    }
                </Swiper>
            </div>
            <div className={styles.moreBox}>
                <Link href="/specialties" title={t('speciality_title',{joinArrays: ' ', name: t('name')})}>
                    <button className={`${styles.more} custom_button`} type="button">
                        <div className={styles.text}>
                            <span>{t('see_specialities')}</span>
                        </div>
                        <div className={`${styles.icon} mirror`}>
                            {SVG.angle_left()}
                        </div>
                    </button>
                </Link>
            </div>
        </section>
    )
}
export default Index