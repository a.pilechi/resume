import PersonalInfo from './components/profilerInfo'
import Detail from './components/detail'
import Specialities from './components/specialties'
import Clients from './components/clients'
import Resume from './components/resume'
import styles from './index.module.scss'

const Index = () => {
    return(
        <main className={styles.main}>
            <div className="container">
                <section className={styles.sides}>
                    <div className={styles.side}>
                        <PersonalInfo />
                    </div>
                    <div className={styles.side}>
                        <Detail />
                    </div>
                </section>
                <Specialities />
                <Clients />
                <Resume />
            </div>
        </main>
    )
}
export default Index