import ContactInfo from './components/contactInfo'
import SocialMedia from './components/socialmedia'
import styles from './index.module.scss'

const Index = () => {
    return(
        <footer className={styles.footer}>
            <div className={`container ${styles.holder}`}>
                <ContactInfo />
                <span className={styles.separator} />
                <SocialMedia />
            </div>
        </footer>
    )
}
export default Index