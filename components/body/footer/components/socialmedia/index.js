import Link from "next/link"
import {useTranslation} from "next-i18next";
import * as SVG from '@/svg'
import SocialMedia from '@/public/data/socialMedia.json'
import styles from './index.module.scss'

const Index = () => {
    const {t} = useTranslation('common');
    return(
        <nav className={styles.social_media}>
            <ul>
                {
                    SocialMedia.map((item,index) =>
                        <li className={styles[item.network]} key={index}>
                            <Link href={item.url}
                                  rel="me noreferrer"
                                  target="_blank"
                                  title={t('x_name',{joinArrays: ' ', x: t(`social_media.${item.network}`), name: t('info.name')})}
                                  aria-label={t('x_name',{joinArrays: ' ', x: t(`social_media.${item.network}`), name: t('info.name')})}
                                  passHref={true}
                                  prefetch={false}>
                                {SVG[item.icon]()}
                            </Link>
                        </li>
                    )
                }
            </ul>
        </nav>
    )
}
export default Index
