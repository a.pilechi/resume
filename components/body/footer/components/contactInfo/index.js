import {useTranslation} from "next-i18next";
import * as SVG from '@/svg'
import Link from "next/link";
import ContactInfo from '@/public/data/contactInfo.json'
import styles from './index.module.scss'

const Index = () => {
    const {t} = useTranslation('common');
    return(
        <nav className={styles.contact_info}>
            <ul>
                {
                    ContactInfo.map((item,index) =>
                        <>
                            {
                                item.visibility &&
                                <li key={index}>
                                    <Link href={item.link}
                                          rel="me noreferrer"
                                          target="_blank"
                                          title={t('x_name',{joinArrays: ' ', x: t(item.id), name: t('info.name')})}
                                          aria-label={t('x_name',{joinArrays: ' ', x: t(item.id), name: t('info.name')})}
                                          passHref={true}
                                          prefetch={false}>
                                        {SVG[item.icon]()}
                                    </Link>
                                </li>
                            }
                        </>
                    )
                }
            </ul>
        </nav>
    )
}
export default Index