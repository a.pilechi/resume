import HomeKey from "./components/keys/home"
import LanguageKey from "./components/keys/language"
import ThemeKey from "./components/keys/theme"
import styles from './index.module.scss'

const Index = () => {
    return(
        <header className={styles.header}>
            <nav className="container">
                <ul>
                    <HomeKey />
                </ul>
                <ul>
                    <LanguageKey />
                    <ThemeKey />
                </ul>
            </nav>
        </header>
    )
}
export default Index