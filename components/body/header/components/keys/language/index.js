import {useState} from "react"
import {useTranslation} from 'next-i18next'
import {language} from '@/svg'
import Modal from "./modal"
import styles from '../index.module.scss'

const Index = () => {
    const {t} = useTranslation('common');
    const [languagesModalVisibility,setLanguagesModalVisibility] = useState(false);
    return(
        <li className={`${styles.key} ${styles.language}`}
             title={t('language')}
             onClick={() => setLanguagesModalVisibility(!languagesModalVisibility)}>
            <div className={styles.icon}>
                {language()}
            </div>
            {
                languagesModalVisibility &&
                <Modal setLanguagesModalVisibility={setLanguagesModalVisibility}/>
            }
        </li>
    )
}
export default Index