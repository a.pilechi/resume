import {useEffect, useRef} from "react"
import {useRouter} from "next/router"
import Link from "next/link"
import {useTranslation} from 'next-i18next'
import * as SVG from '@/svg'
import styles from './index.module.scss'

const Index = ({setLanguagesModalVisibility}) => {
    const {t} = useTranslation('common');
    const router = useRouter();
    const dropdown = useRef(null);
    useEffect(() => {
        const handleClickOutside = event => {
            if (dropdown.current && !dropdown.current.contains(event.target)) {
                setLanguagesModalVisibility(false);
            }
        };
        document.addEventListener("mouseup", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        }
    }, [dropdown])
    return(
        <div className={styles.languagesModal} ref={dropdown}>
            {
                router.locales.map((locale,index) =>
                    <div className={styles.language} key={index}>
                        <Link href={router.asPath} locale={locale}>
                            <div className={styles.icon}>
                                {SVG[`flag_${locale}`]()}
                            </div>
                            <div className={styles.text}>
                                <span>{t(`languages.${locale}`)}</span>
                            </div>
                        </Link>
                    </div>
                )
            }
        </div>
    )
}
export default Index