import Link from 'next/link'
import {useTranslation} from "next-i18next";
import {home} from '@/svg'
import styles from '../index.module.scss'

const Index = () => {
    const {t} = useTranslation('common');
    return(
        <li className={`${styles.key} ${styles.home}`}>
            <Link href="/"
                  title={t('x_name', {joinArrays: ' ', x: t('resume'), name: t('info.name')})}
                  aria-label={t('x_name', {joinArrays: ' ', x: t('resume'), name: t('info.name')})}>
                <div className={styles.icon}>
                    {home()}
                </div>
            </Link>
        </li>
    )
}
export default Index