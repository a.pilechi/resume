import {useState} from "react";
import {useTranslation} from 'next-i18next'
import {moon, sun} from '@/svg'
import styles from '../index.module.scss'

const Index = () => {
    const {t} = useTranslation('common');
    const [toggle,setToggle] = useState(true);
    return(
        <li className={`${styles.key} ${styles.grayscale_theme}`}
             onClick={() => setToggle(!toggle)}
             title={t(toggle ? 'dark_theme' : 'light_theme')}>
            <div className={styles.icon}>
                {toggle ? moon() : sun()}
            </div>
            <style jsx global>{`
                :root{
                    --grayscale: ${toggle ? 'black' : 'white'};
                    --grayscale-gradient1: ${toggle ? 'rgba(10, 20, 25, 0.5)' : 'rgb(255, 255, 255, 0.5)'};
                    --grayscale-gradient2: ${toggle ? 'rgba(0, 208, 255, 0.17)' : 'rgba(0, 0, 0, 0.17)'};
                    --grayscale-gradient3: ${toggle ? 'rgba(10, 20, 25, 0.5)' : 'rgba(255, 255, 255, 0.5)'};
                    --grayscale-transparent1: ${toggle ? 'rgba(0, 0, 0, 0.3)' : 'rgba(255, 255, 255, 0.3)'};
                    --grayscale-transparent2: ${toggle ? 'rgba(51, 51, 51, 0.2)' : 'rgba(51, 51, 51, 0.2)'};
                    --background: ${toggle ? "url('/images/background/pattern_dark.png')" : "url('/images/background/pattern_light.png')"};
                    --grayscale-background: ${toggle ? '#010101' : '#ebebeb'};
                    --grayscale-color: ${toggle ? '#c1c1c1' : '#666666'};
                }
            `}</style>
        </li>
    )
}
export default Index