import { useEffect } from "react";
import getConfig from "next/config";
import Head from "next/head"
import Script from 'next/script'
import { useRouter } from 'next/router';
import { appWithTranslation } from 'next-i18next'
import nextI18NextConfig from '../next-i18next.config.js'
import NextNProgress from 'nextjs-progressbar'
import * as gtag from '@/lib/gtag'
import "@/styles/global/index.scss"

const { pwa } = getConfig().publicRuntimeConfig;

function App({ Component, pageProps }) {
    const router = useRouter();
    useEffect(() => {
        const handleRouteChange = (url) => {
            gtag.pageview(url)
        }
        router.events.on('routeChangeComplete', handleRouteChange)
        return () => {
            router.events.off('routeChangeComplete', handleRouteChange)
        }
    }, [router.events])
    return (
        <>
            <Head>
                <meta charSet='utf-8'/>
                <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                <meta name="theme-color" content={pwa.theme}/>
                <meta name='viewport' content='initial-scale=1.0, width=device-width'/>
                <meta name="mobile-web-app-capable" content="yes"/>
                <meta name="apple-mobile-web-app-title" content={pwa.name} data-n-head="true" data-hid="apple-mobile-web-app-title"/>
                <meta name="apple-mobile-web-app-capable" content="yes"/>
                <meta name="apple-mobile-web-app-status-bar-style" content={pwa.theme}/>
                <meta name="apple-mobile-web-app-title" content={pwa.name}/>
                <meta name="msapplication-navbutton-color" content={pwa.theme}/>
                <meta name="msapplication-TileColor" content={pwa.theme}/>
                <meta name="msapplication-TileImage" content={pwa.logo}/>
                <meta name="msapplication-config" content="/browserconfig.xml"/>
                <meta name="application-name" content={pwa.name}/>
                <meta name="msapplication-tooltip" content="Tooltip Text"/>
                <meta name="msapplication-starturl" content="/"/>
                <meta name="msapplication-tap-highlight" content="no"/>
                <meta name="full-screen" content="yes"/>
                <meta name="browsermode" content="application"/>
                <meta name="nightmode" content="enable/disable"/>
                <meta name="screen-orientation" content="portrait"/>
                <meta name="google-site-verification" content="BldIkFQwNnaoKIvGE5PExkdHIbBB3b5LzCh-d20ZIUA" />
                <link rel="icon" href="/images/icon/favicon.ico" />
                <link rel="manifest" href="/manifest.json" crossOrigin="use-credentials"/>
                <link rel="shortcut icon" href={pwa.logo}/>
                <link rel='mask-icon' href={pwa.types.svg} color={pwa.theme} />
                <link rel="apple-touch-icon" href={pwa.logo} />
                <link rel='icon' type='image/png' sizes='16x16' href={pwa.sizes["16"]} />
                <link rel='icon' type='image/png' sizes='32x32' href={pwa.sizes["32"]} />
                <link rel="apple-touch-icon" sizes="72x72" href={pwa.sizes["72"]} />
                <link rel="apple-touch-icon" sizes="120x120" href={pwa.sizes["120"]} />
                <link rel="apple-touch-icon" sizes="144x144" href={pwa.sizes["144"]} />
                <link rel="apple-touch-icon" sizes="180x180" href={pwa.sizes["180"]} />
            </Head>
            <Script
                strategy="afterInteractive"
                src={`https://www.googletagmanager.com/gtag/js?id=${gtag.GA_TRACKING_ID}`}
            />
            <Script
                id="gtag-init"
                strategy="afterInteractive"
                dangerouslySetInnerHTML={{
                    __html: `
                        window.dataLayer = window.dataLayer || [];
                        function gtag(){dataLayer.push(arguments);}
                        gtag('js', new Date());
                        gtag('config', '${gtag.GA_TRACKING_ID}', {
                          page_path: window.location.pathname,
                        });
                      `,
                }}
            />
            <Script id="google-analytics" strategy="afterInteractive">
                {`
                    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-M6W7SW5');
                `}
            </Script>
            <NextNProgress color="#29D" />
            <Component {...pageProps} />
        </>
    )
}
export default appWithTranslation(App, nextI18NextConfig)