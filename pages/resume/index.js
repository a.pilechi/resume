import Head from "next/head";
import {useTranslation} from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import {CommonJsonLd} from "@/schema"
import Layout from '@/layout'
import Main from '@/main/resume'

export default function Resume() {
    const {t} = useTranslation('resume');
    return (
        <>
            <Head>
                <title>{t('seo.title')}</title>
                <meta name="description" content={t('seo.description')} />
                <meta name="keywords" content={t('common:seo.keywords',{joinArrays: ','})} />
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={CommonJsonLd()}
                    key="common-jsonld"
                />
            </Head>
            <Layout>
                <Main />
            </Layout>
        </>
    )
}
export const getStaticProps = async ({ locale }) => {
    return{
        props: {
            ...await serverSideTranslations(locale, ['common','resume']),
        },
    }
}