import Head from "next/head";
import {useRouter} from 'next/router'
import {useTranslation} from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import {CommonJsonLd} from "@/schema"
import Samples from '@/public/data/samples.json'
import Layout from '@/layout'
import Main from '@/main/sample'

export default function Sample() {
    const { t } = useTranslation('samples');
    const { query: {id} } = useRouter();
    return (
        <>
            <Head>
                <title>{t('seo.sample.title',{joinArrays: ' ', name: t(`common:samples.${id}.name`)})}</title>
                <meta name="description" content={t('seo.description')} />
                <meta name="keywords" content={t('common:seo.keywords',{joinArrays: ','})} />
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={CommonJsonLd()}
                    key="common-jsonld"
                />
            </Head>
            <Layout>
                <Main />
            </Layout>
        </>
    )
}
export const getStaticProps = async ({ locale }) => {
    return{
        props: {
            ...await serverSideTranslations(locale, ['common','samples']),
        },
    }
}
export async function getStaticPaths({ locales }) {
    let paths = [];
    locales?.map((locale) => {
        Samples?.filter(({project}) => {
            return project
        })?.map(({status, id}) => {
            paths.push({ params: { status, id }, locale })
        })
    })
    return {
        paths,
        fallback: true,
    }
}