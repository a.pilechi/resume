import Head from "next/head";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import styles from "../styles/offline/index.module.scss";

function Offline() {
    const { t } = useTranslation('offline');
    return(
        <>
            <Head>
                <title>{`${t('seo.title')} | ${t('common:owner.companyName')}`}</title>
                <meta name="robots" content="noindex"/>
                <meta name="googlebot" content="noindex" />
            </Head>
            <main className={styles.offline}>
                <div className={styles.image}>
                    <img src="/images/background/noConnection.png" />
                </div>
                <p className={styles.title}>{t('str1')}</p>
                <p className={styles.subtitle}>{t('str2')}</p>
            </main>
        </>
    )
}

export async function getServerSideProps({ locale }) {
    return {
        props: {
            ...await serverSideTranslations(locale, ['common','offline'])
        }
    }
}
export default Offline