/** @type {import('next-sitemap').IConfig} */
module.exports = {
    siteUrl: process.env.SITE_URL || 'https://arash-pilechi.vercel.app',
    generateRobotsTxt: true,
    // changefreq: 'weekly'
}